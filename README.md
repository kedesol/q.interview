## CRM System for Qantas program test

This is a maven project developed using Java 8 programming language and spring frameworks to expost REST APIs for 
CRM system. Please follow the steps to run the program locally

1. You need Java 8 installed on your local machine, and maven 3.0 also installed on your local machine, along with git
2. Checkout this public repo onto your local machine using git
3. Run the command 'mvn package'
4. If the build is successful then run the command 'mvn spring-boot:run' from the project root and it will run the spring boot application on http://localhost:8080
5. This application is password protected, and that password would be displayed while the spring boot is starting the web container
6. Once application is running you can access various customer related APIs