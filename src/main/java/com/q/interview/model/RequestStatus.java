package com.q.interview.model;

public enum RequestStatus {
    SUCCESSFUL, NOT_FOUND, VALIDATION_FAILED;
}
