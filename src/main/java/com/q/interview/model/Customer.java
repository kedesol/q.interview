package com.q.interview.model;

import java.time.LocalDate;

/**
 * This class is a DTO for Customer model
 */
public class Customer {
    private String id;

    private String firstName;
    private String lastName;
    private LocalDate dob;
    private Address homeAddress;
    private Address officeAddress;
    private String email;

    public Customer() {
    }

    public Customer(String firstName, String lastName, LocalDate dob, Address homeAddress,
                    Address officeAddress, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
        this.homeAddress = homeAddress;
        this.officeAddress = officeAddress;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public Customer setId(String id) {
        this.id = id;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDate getDob() {
        return dob;
    }

    public Address getHomeAddress() {
        return homeAddress;
    }

    public Address getOfficeAddress() {
        return officeAddress;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dob=" + dob +
                ", homeAddress=" + homeAddress +
                ", officeAddress=" + officeAddress +
                ", email='" + email + '\'' +
                '}';
    }
}
