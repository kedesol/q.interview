package com.q.interview.model;

/**
 * This class is a DTO for Address model
 */
public class Address {
    private String plotNumber;
    private String street;
    private String area;
    private String city;
    private String state;
    private int pin;

    public Address() {
    }

    public Address(String plotNumber, String street, String area, String city, String state, int pin) {
        this.plotNumber = plotNumber;
        this.street = street;
        this.area = area;
        this.city = city;
        this.state = state;
        this.pin = pin;
    }

    public String getPlotNumber() {
        return plotNumber;
    }

    public String getStreet() {
        return street;
    }

    public String getArea() {
        return area;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public int getPin() {
        return pin;
    }

    @Override
    public String toString() {
        return "Address{" +
                "plotNumber='" + plotNumber + '\'' +
                ", street='" + street + '\'' +
                ", area='" + area + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", pin=" + pin +
                '}';
    }
}
