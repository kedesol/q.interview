package com.q.interview.service;

import com.q.interview.model.Address;
import com.q.interview.model.Customer;
import com.q.interview.model.RequestStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

import static com.q.interview.util.StringUtils.isNotEmpty;

@Service
public class CustomerService {

    private static final Logger LOGGER = LogManager.getLogger(CustomerService.class);

    /**
     * This method creates a new customer with the given {@link Customer} object
     *
     * @param customer
     * @return customer ID of the newly generated customer object, NULL if the validation failed
     */
    public String createCustomer(Customer customer) {
        boolean isValid = validateCustomer(customer);
        if (!isValid) {
            return null;
        }
        String id = getRandomId();
        customer.setId(id);
        LOGGER.info("A new {} is generated with the ID {}", customer, id);
        return id;
    }

    /**
     * This method updates an existing customer with the given {@link Customer} object
     *
     * @param customer
     * @return RequestStatus instance based on the whether operation was successful or not
     */
    public RequestStatus updateCustomer(Customer customer) {
        if (isNotEmpty(customer.getId())) {
            boolean isValid = validateCustomer(customer);
            if (!isValid) {
                return RequestStatus.VALIDATION_FAILED;
            }
            LOGGER.info("Updating the {}", customer);
            return RequestStatus.SUCCESSFUL;
        } else {
            return RequestStatus.VALIDATION_FAILED;
        }
    }

    public Customer getCustomer(String id) {
        return new Customer("Fu", "Bar", LocalDate.of(2010, 8, 24),
                new Address("24", "George St", "Sydney", "Sydney", "NSW", 2000),
                new Address("24", "Pitt St", "Sydney", "Sydney", "NSW", 2000),
                "fu@bar.com");
    }

    /**
     * This method deletes an existing customer with the given id
     *
     * @param id
     * @return RequestStatus instance based on the whether operation was successful or not
     */
    public RequestStatus deleteCustomer(String id) {
        if (isNotEmpty(id)) {
            LOGGER.info("Deleting the customer with ID {}", id);
            return RequestStatus.SUCCESSFUL;
        } else {
            return RequestStatus.VALIDATION_FAILED;
        }
    }

    private boolean validateCustomer(Customer customer) {
        boolean fieldsNotEmpty = isNotEmpty(customer.getFirstName())
                && isNotEmpty(customer.getLastName())
                && isNotEmpty(customer.getEmail());
        boolean homeAddressNotEmpty = validateAddress(customer.getHomeAddress());
        boolean officeAddressNotEmpty = validateAddress(customer.getOfficeAddress());
        boolean addressAvailable = homeAddressNotEmpty || officeAddressNotEmpty;
        return fieldsNotEmpty && addressAvailable;
    }

    private boolean validateAddress(Address address) {
        if (address == null) {
            return false;
        }
        boolean fieldsNotEmpty = isNotEmpty(address.getStreet())
                && isNotEmpty(address.getArea())
                && isNotEmpty(address.getCity())
                && isNotEmpty(address.getState())
                && address.getPin() != 0;
        return fieldsNotEmpty;
    }

    private String getRandomId() {
        return String.valueOf((int) (Math.random() * ((100000 - 1) + 1)) + 1);
    }
}
