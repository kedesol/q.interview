package com.q.interview.util;

import com.q.interview.model.Address;
import com.q.interview.model.Customer;

import static com.q.interview.util.StringUtils.isNotEmpty;

public class Validator {

    /**
     * This method validates the customer instance for not empty checks
     *
     * All the fields like first and last name, email should not be empty, date of birth should not be null,
     * and either of the address must be available
     *
     * @param customer
     * @return
     */
    public static boolean validateCustomer(Customer customer) {
        boolean fieldsNotEmpty = isNotEmpty(customer.getFirstName())
                && isNotEmpty(customer.getLastName())
                && isNotEmpty(customer.getEmail());
        boolean dobNotNull = customer.getDob() != null;
        boolean homeAddressNotEmpty = validateAddress(customer.getHomeAddress());
        boolean officeAddressNotEmpty = validateAddress(customer.getOfficeAddress());
        boolean addressAvailable = homeAddressNotEmpty || officeAddressNotEmpty;
        return fieldsNotEmpty && addressAvailable && dobNotNull;
    }

    /**
     * This method checks for the not empty address fields like street, area, city and state and pin must
     * not be 0
     *
     * @param address
     * @return
     */
    public static boolean validateAddress(Address address) {
        if (address == null) {
            return false;
        }
        boolean fieldsNotEmpty = isNotEmpty(address.getStreet())
                && isNotEmpty(address.getArea())
                && isNotEmpty(address.getCity())
                && isNotEmpty(address.getState())
                && address.getPin() != 0;
        return fieldsNotEmpty;
    }

}
