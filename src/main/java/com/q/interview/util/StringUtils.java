package com.q.interview.util;

public class StringUtils {

    /**
     * This method checks if the given string is null or empty or blank string if so then returns true
     * otherwise false
     *
     * @param str
     * @return
     */
    public static boolean isNotEmpty(String str) {
        return str != null ? !str.trim().equals("") : false;
    }
}
