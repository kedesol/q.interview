package com.q.interview.controller;

import com.q.interview.model.Customer;
import com.q.interview.model.RequestStatus;
import com.q.interview.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<String> createCustomer(@RequestBody Customer customer) {
        String customerId = this.customerService.createCustomer(customer);
        if (customerId == null) {
            new ResponseEntity<>(customerId, HttpStatus.EXPECTATION_FAILED);
        }
        return new ResponseEntity<>(customerId, HttpStatus.OK);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResponseEntity<String> updateCustomer(@RequestBody Customer customer) {
        RequestStatus status = this.customerService.updateCustomer(customer);
        String result;
        ResponseEntity<String> responseEntity;
        switch (status) {
            case SUCCESSFUL:
                result = "Customer updated successfully";
                responseEntity = new ResponseEntity<>(result, HttpStatus.OK);
                break;
            case NOT_FOUND:
                result = "Customer with the given ID not found";
                responseEntity = new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
                break;
            default:
                responseEntity = new ResponseEntity<>("FAILED", HttpStatus.FAILED_DEPENDENCY);
                break;
        }
        return responseEntity;
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<Customer> getCustomer(@PathVariable String id) {
        Customer customer = this.customerService.getCustomer(id);
        return new ResponseEntity<>(customer, HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteCustomer(@PathVariable String id) {
        RequestStatus status = this.customerService.deleteCustomer(id);
        String result;
        ResponseEntity<String> responseEntity;
        switch (status) {
            case SUCCESSFUL:
                result = "Customer deleted successfully";
                responseEntity = new ResponseEntity<>(result, HttpStatus.OK);
                break;
            case NOT_FOUND:
                result = "Customer with the given ID not found";
                responseEntity = new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
                break;
            default:
                responseEntity = new ResponseEntity<>("FAILED", HttpStatus.FAILED_DEPENDENCY);
                break;
        }
        return responseEntity;
    }
}
