package com.q.interview.util;

import com.q.interview.model.Address;
import com.q.interview.model.Customer;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ValidatorTest {

    @Test
    public void testValidateAddress() {
        Address a = new Address("", "", "", "", "", 0);
        assertFalse(Validator.validateAddress(a));

        a = new Address("a", "b", "c", "d", "e", 2000);
        assertTrue(Validator.validateAddress(a));
    }

    @Test
    public void testValidateCustomer() {
        Address homeAddress = new Address("P", "S", "A", "C", "S", 2000);
        Customer c = new Customer("F", "L", LocalDate.of(2000, 1, 1), homeAddress, null, "a@b.com");
        assertTrue(Validator.validateCustomer(c));

        c = new Customer("F", "L", LocalDate.of(2000, 1, 1), null, null, "a@b.com");
        assertFalse(Validator.validateCustomer(c));

        c = new Customer("", "", null, null, null, "a@b.com");
        assertFalse(Validator.validateCustomer(c));
    }

}
