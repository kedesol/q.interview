package com.q.interview.service;

import com.q.interview.model.Address;
import com.q.interview.model.Customer;
import com.q.interview.model.RequestStatus;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;

public class CustomerServiceTest {

    private CustomerService customerService;

    @Before
    public void setup() {
        this.customerService = new CustomerService();
    }

    @Test
    public void testCreateCustomer_Validation_Failed() {
        Customer c = new Customer("", "", null, null, null, "a@b.com");
        String result = this.customerService.createCustomer(c);
        assertNull(result);
    }

    @Test
    public void testCreateCustomer() {
        Address homeAddress = new Address("P", "S", "A", "C", "S", 2000);
        Customer c = new Customer("F", "L", LocalDate.of(2000, 1, 1), homeAddress, null, "a@b.com");
        assertNull(c.getId());
        String result = this.customerService.createCustomer(c);
        assertNotNull(result);
        String id = c.getId();
        int intId = Integer.parseInt(id);
        assertTrue(intId > 0);
    }

    @Test
    public void testUpdateCustomer() {
        Address homeAddress = new Address("P", "S", "A", "C", "S", 2000);
        Customer c = new Customer("F", "L", LocalDate.of(2000, 1, 1), homeAddress, null, "a@b.com");
        c.setId("5678");
        RequestStatus status = this.customerService.updateCustomer(c);
        assertNotNull(status);
        assertEquals(RequestStatus.SUCCESSFUL, status);
    }

    @Test
    public void testUpdateCustomer_Id_Null() {
        Address homeAddress = new Address("P", "S", "A", "C", "S", 2000);
        Customer c = new Customer("F", "L", LocalDate.of(2000, 1, 1), homeAddress, null, "a@b.com");
        RequestStatus status = this.customerService.updateCustomer(c);
        assertNotNull(status);
        assertEquals(RequestStatus.VALIDATION_FAILED, status);
    }

    @Test
    public void testUpdateCustomer_Other_Fields_Not_Available() {
        Address homeAddress = new Address("P", null, "A", "C", "S", 2000);
        Customer c = new Customer("F", null, LocalDate.of(2000, 1, 1), homeAddress, null, "a@b.com");
        RequestStatus status = this.customerService.updateCustomer(c);
        assertNotNull(status);
        assertEquals(RequestStatus.VALIDATION_FAILED, status);
    }

    @Test
    public void testDeleteCustomer_Id_Null() {
        RequestStatus status = this.customerService.deleteCustomer(null);
        assertNotNull(status);
        assertEquals(RequestStatus.VALIDATION_FAILED, status);
    }

    @Test
    public void testDeleteCustomer() {
        RequestStatus status = this.customerService.deleteCustomer("4567");
        assertNotNull(status);
        assertEquals(RequestStatus.SUCCESSFUL, status);
    }

}
